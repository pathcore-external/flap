from json import *
import os
import sqlite3
import smtplib, ssl
import time

from flask import Flask, Response, abort, request

app = Flask(__name__)


def get(con, cur, cdscode, flag):

    if flag:
        schools = []
        for row in cur.execute(
            f"SELECT * FROM schools WHERE CDSCode='{cdscode}' LIMIT 1"
        ):
            hit = dict(row)
            schools.append(hit)

        if not schools:
            rsp = {"msg": f"school CDSCode {cdscode} not found", "http": 404}
            abort(Response(dumps(rsp), rsp["http"]))

        return schools[0]

    if not flag:
        score = None
        for row in cur.execute(f"SELECT * FROM satscores WHERE cds='{cdscode}'"):
            score = dict(row)
            break

        if not score:
            rsp = {"msg": f"scores for school CDSCode {cdscode} not found", "http": 404}
            abort(Response(dumps(rsp), rsp["http"]))

        return score


def project(keys, source, destination={}):

    for i in range(0, len(keys)):
        value = source.get(keys[i], None)
        if value != None:
            destination[keys[i]] = source[keys[i]]

    return destination


@app.route("/api")
def identification():
    return {
        "application": "flap",
        "version": "0.0.1",
    }


@app.route("/schools", methods=["POST"])
def add_school():

    record = request.json

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cols = []
    vals = []
    for k, v in record.items():
        cols.append(str(k))
        vals.append('"' + str(v) + '"')

    cols = ",".join(cols)
    vals = ",".join(vals)

    query = "INSERT INTO schools (" + cols + ") VALUES(" + vals + ");"

    cur.executescript(query)
    con.commit()
    con.close()

    return ("", 204)


@app.route("/api/schools/<cdscode>", methods=["GET"])
def get_school(cdscode):

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    result = get(con, cur, cdscode, True)

    result["local"] = (
        False
        if result["Latitude"] == None
        else False
        if result["Latitude"] > 38
        else False
        if result["Latitude"] < 36
        else False
        if result["Longitude"] > -122
        else False
        if result["Longitude"] < -123
        else True
    )

    con.close()

    return result


@app.route("/api/scores/<cdscode>", methods=["GET"])
def scores(cdscode):

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    result = get(con, cur, cdscode, False)

    con.close()

    return result


@app.route("/api/list/<int:skip>/<int:take>", methods=["GET"])
def list_schools(skip, take):

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    hits = []
    more = False

    for row in cur.execute(
        f"SELECT * FROM schools ORDER BY CDSCode LIMIT {take+1} OFFSET {skip}"
    ):
        if len(hits) < take:
            hit = dict(row)

            if hit["Latitude"] is not None:
                if hit["Latitude"] < 38:
                    if hit["Latitude"] > 36:
                        if hit["Longitude"] < -122:
                            if hit["Longitude"] > -123:
                                hit["local"] = True

            hit["contacts"] = project(["AdmEmail1", "AdmEmail2", "AdmEmail3"], hit)

            # TODO

            hits.append(hit)

        else:
            more = True

    con.close()

    return {
        "hits": hits,
        "more": more,
    }


@app.route("/schools/<cdscode>", methods=["PUT"])
def modify_school(cdscode):

    record = request.json

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    vals = []
    for k in record.keys():
        v = record[k]
        if isinstance(v, float) or isinstance(v, int):
            vals.append(f"{str(k)}={str(v)}")
        elif isinstance(v, str):
            vals.append(f'{str(k)}="{str(v)}"')
        else:
            raise ValueError("unexpected value")

    vals = ",".join(vals)

    try:
        cur.execute(f"UPDATE schools SET {vals} WHERE CDSCode='{cdscode}'")
    except:
        pass

    con.commit()
    con.close()

    return {
        "count": cur.rowcount,
    }


@app.route("/api/school/<cdscode>", methods=["DELETE"])
def delete_school(cdscode):

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cur.executescript(f"DELETE FROM schools WHERE CDSCode='{cdscode}';")
    con.commit()
    con.close()

    return ("", 204)


@app.route("/api/school/report", methods=["POST"])
def report():

    start = time.time()
    print("generating report")

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    filename = os.path.join(os.path.dirname(__file__), "cdeschools.csv")
    report = open(filename, "w")

    keys = None

    index = 0
    for row in cur.execute(f"SELECT * FROM schools ORDER BY CDSCode"):
        hit = dict(row)

        if not keys:
            keys = sorted(hit.keys())
            report.write("Index," + ",".join(keys) + "\n")

        values = [str(index)]
        for k in keys:
            if hit[k] == None:
                values.append("")
            elif type(hit[k]) == str:
                values.append('"' + str(hit[k]) + '"')
            else:
                values.append(str(hit[k]))
        values = ",".join(values) + "\n"

        report.write(values)

        index += 1

    con.close()

    report.close()

    end = time.time()
    print("elasped time " + str(end - start))

    return ("", 204)


@app.route("/api/school/<cdscode>/notify", methods=["POST"])
def notify(cdscode):

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    schools = []
    for row in cur.execute(f"SELECT * FROM schools WHERE CDSCode='{cdscode}' LIMIT 1"):
        hit = dict(row)
        schools.append(hit)

    if len(schools) == 0:
        rsp = {"msg": f"school CDSCode {cdscode} not found", "http": 404}
        abort(Response(dumps(rsp), rsp["http"]))

    school = schools[0]

    # NOTE TO APPLICANTS
    # the names and servers are intentionlly set to not exist to avoid sending emails to real people

    emails = []
    if school["AdmEmail1"] is not None:
        emails.append("AdmEmail1@pathcore.com")
    if school["AdmEmail2"] is not None:
        emails.append("AdmEmail2@pathcore.com")
    if school["AdmEmail3"] is not None:
        emails.append("AdmEmail3@pathcore.com")

    if not emails:
        return ("", 204)

    print("sending notification to " + ", ".join(emails))

    fromaddr = "testing@pathcore.com"

    msg = (
        "From: "
        + fromaddr
        + "\r\nTo: "
        + ", ".join(emails)
        + "\r\n\r\nNOTIFICATION!\r\n\r\n"
    )

    server = None
    try:
        server = smtplib.SMTP("smtp.pathcore.com")
        server.sendmail(fromaddr, emails, msg)
    finally:
        if server:
            server.quit()

    return ("", 204)


@app.route("/api/school/grid", methods=["GET"])
def grid():

    la1 = float(request.args.get("la1", -90.0))
    la2 = float(request.args.get("la2", 90.0))
    lo1 = float(request.args.get("lo1", -180.0))
    lo2 = float(request.args.get("lo2", 180.0))

    path = os.path.join(os.path.dirname(__file__), "cdeschools.sqlite")
    con = sqlite3.connect(path)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    query = cur.execute(f"SELECT * FROM schools ORDER BY CDSCode")

    hits = []

    while 1:

        hit = query.fetchone()
        if hit is None:
            return {"hits": hits}
        hit = dict(hit)

        lo = hit.get("Longitude")
        la = hit.get("Latitude")

        hit = {
            "CDSCode": hit.get("CDSCode"),
            "Longitude": la,
            "Latitude": lo,
        }

        if lo1 <= float(lo or -9999.0) <= lo2:
            if la1 <= float(la or -9999.0) <= la2:
                hits.append(hit)

    return ("", 204)
