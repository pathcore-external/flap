# flap

Flap is an **intentionally bad** Flask application for training, assessment and discussion.


# run
```
pipenv install
pipenv run flask run
```

# example API requests
```
curl -s http://127.0.0.1:5000/api | jq

curl -s -H 'Content-type: application/json' http://127.0.0.1:5000/schools -d '{"CDSCode":555, "StatusType":"Active","County":"Alameda","District":"Alameda","DOC":"00","DOCType":"County","LastUpdate":"2019-01-01"}' | jq

curl -s http://127.0.0.1:5000/api/schools/555 | jq

curl -s http://127.0.0.1:5000/api/scores/555 | jq

curl -s http://127.0.0.1:5000/api/list/100/5 | jq

curl -s -X PUT -H 'Content-type: application/json' http://127.0.0.1:5000/schools/555 -d '{"StatusType":"Closed"}' | jq

curl -s -X DELETE http://127.0.0.1:5000/api/school/555 | jq

curl -s -X POST http://127.0.0.1:5000/api/school/report

curl -s -X POST http://127.0.0.1:5000/api/school/555/notify

curl -s http://127.0.0.1:5000/api/school/grid?la0=-100&la1=100&la0=-100&la1=100 | jq
``` 
